﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCoin.Models
{
    public class UserModel : BaseEntity
    {
        public Role Role { get; set; }

        [Required, MaxLength(36)]
        public string Login { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime RegisterAt { get; set; }

        [ConcurrencyCheck]
        public int Balance { get; set; }

        protected UserModel()
        {
        }

        public UserModel(string login, Role role) => (Login, Role) = (login, role);

        public UserModel(string login, Role role, int balance) : this(login, role) => Balance = balance;
    }
}