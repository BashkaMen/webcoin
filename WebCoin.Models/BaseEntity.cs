﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCoin.Models
{
    public abstract class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}