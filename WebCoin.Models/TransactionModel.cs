﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCoin.Models
{
    public class TransactionModel : BaseEntity
    {
        public Guid From { get; set; }
        public Guid To { get; set; }
        public int Amount { get; set; }
        public int Fee { get; set; }
        public TransactionType Type { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; set; }

        protected TransactionModel()
        {
        }

        public TransactionModel(Guid userIdFrom, Guid userIdTo, TransactionType type, int amount, int fee)
        {
            From = userIdFrom;
            To = userIdTo;
            Type = type;
            Amount = amount;
            Fee = fee;
        }
    }
}