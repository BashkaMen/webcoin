﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebCoin.Api.Helpers;
using WebCoin.Models;
using WebCoin.Core.Services;

namespace WebCoin.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService userService;
        private readonly MessageSender messageSender;

        public UserController(UserService userService, MessageSender messageSender)
        {
            this.userService = userService;
            this.messageSender = messageSender;
        }

        [AllowAnonymous]
        [HttpPost("signin/{phone}")]
        public async Task SendOtp(string phone)
        {
            if (!Regex.IsMatch(phone, @"^\d{11,12}$"))
            {
                throw new ArgumentException("Не верный номер телефона");
            }

            var otp = CommonExtensions.GenerateOTP();

            HttpContext.Session.SetString("otp", otp);
            HttpContext.Session.SetString("phone", phone);

            await messageSender.SendMessage($"Your otp code: *{otp}*");
        }

        [AllowAnonymous]
        [HttpPost("signin/validate/{otp}")]
        public async Task<TokenModel> ValidateOtp(string otp)
        {
            var phone = HttpContext.Session.GetString("phone");
            var expectedOtp = HttpContext.Session.GetString("otp");

            if (phone == null) throw new ApplicationException("Возникла ошибка, попробуйте заново");
            if (expectedOtp != otp) throw new ApplicationException("Не верный код");

            var user = await userService.GetOrCreate(phone);

            return user.MakeToken();
        }
    }
}