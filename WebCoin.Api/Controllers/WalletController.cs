﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebCoin.Models;
using WebCoin.Core.Services;

namespace WebCoin.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        private readonly TransactionService transactionService;
        private readonly UserService userService;

        public WalletController(TransactionService transactionService, UserService userService)
        {
            this.transactionService = transactionService;
            this.userService = userService;
        }

        [HttpPost("send/{to}/{amount}")]
        public async Task<TransactionModel> SendMoney(Guid to, int amount)
        {
            var from = User.GetId();

            if (amount <= 0) throw new ApplicationException("Не верная сумма перевода");
            if (!await userService.HasOnBalanceAsync(from, amount + AppSettings.Fee)) throw new ApplicationException("Недостаточно средсв на балансе");
            if (!await userService.UserExistAsync(to)) throw new ApplicationException("Получатель не найден");

            return await transactionService.Transfer(from, to, amount, AppSettings.Fee);
        }

        [HttpGet("balance")]
        public Task<int> GetBalance()
        {
            return userService.GetBalanceAsync(User.GetId());
        }

        [HttpGet("history/{offset}/{count}")]
        public Task<IEnumerable<TransactionModel>> TransactionHistory(int offset, int count)
        {
            return transactionService.GetUserTransactionsAsync(User.GetId(), offset, count);
        }
    }
}