﻿using System;
using System.Linq;
using System.Security.Claims;
using WebCoin.Models;

namespace WebCoin.Api
{
    public static class CommonExtensions
    {
        private static Random random = new Random();

        public static Guid GetId(this ClaimsPrincipal claims) => Guid.Parse(claims.Identity.Name);

        public static Role GetRole(this ClaimsPrincipal claims) => Enum.Parse<Role>(claims.Claims.FirstOrDefault(s => s.Type == ClaimTypes.Role).Value);

        public static string GenerateOTP() => random.Next(1000, 9999).ToString();
    }
}