﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebCoin.Api.Filters
{
    public class HandleError : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var env = context.HttpContext.RequestServices.GetService(typeof(IHostingEnvironment)) as IHostingEnvironment;
            var message = "Возникла ошибка";

            if (context.Exception is ApplicationException || context.Exception is ArgumentException)
            {
                message = context.Exception.Message;
            }
            else
            {
                if (env.IsDevelopment())
                {
                    message = context.Exception.Message + "\n" + context.Exception.StackTrace;
                }
            }

            context.Result = new ContentResult
            {
                Content = message,
                StatusCode = StatusCodes.Status500InternalServerError
            };

            context.ExceptionHandled = true;
        }
    }
}