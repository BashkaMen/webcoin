﻿using Microsoft.Extensions.Configuration;

namespace WebCoin.Api
{
    public class AppSettings
    {
        public static IConfiguration Configuration { get; set; }

        public static string TokenSecretKey => Configuration["TokenSecretKey"];

        public static string MainConnectionString => Configuration["MainConnectionString"];

        public static int Fee => int.Parse(Configuration["Fee"]);
    }
}