﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WebCoin.Core.Services;

namespace WebCoin.Core
{
    public static class Configure
    {
        public static void AddWebCoinCore(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<MainContext>(opt =>
            {
                opt.UseMySql(connectionString, x =>
                {
                    x.ServerVersion("8.0");
                    x.EnableRetryOnFailure();
                });
            });

            services.AddTransient<UserService>();
            services.AddTransient<TransactionService>();
            services.AddTransient<MessageSender>();
        }
    }
}