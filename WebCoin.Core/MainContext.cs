﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebCoin.Models;

namespace WebCoin.Core
{
    public class MainContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }
        public DbSet<TransactionModel> Transactions { get; set; }

        public MainContext(DbContextOptions options) : base(options)
        {
        }
    }
}