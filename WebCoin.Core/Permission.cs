﻿using System;
using System.Collections.Generic;
using System.Text;
using WebCoin.Models;

namespace WebCoin.Core
{
    public static class PermissionRules
    {
        private static Dictionary<Role, List<Permission>> rules = new Dictionary<Role, List<Permission>>();

        static PermissionRules()
        {
            AddUserPermission();
            AddAdminPermission();
        }

        private static void AddAdminPermission()
        {
            
        }

        private static void AddUserPermission()
        {
            rules.Add(Role.User, new List<Permission>
            {
                Permission.P2P,
            });
        }

    }
}
