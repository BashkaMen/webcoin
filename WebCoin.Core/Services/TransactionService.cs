﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCoin.Models;

namespace WebCoin.Core.Services
{
    public class TransactionService
    {
        private readonly MainContext context;

        public TransactionService(MainContext context)
        {
            this.context = context;
        }

        public async Task<TransactionModel> Transfer(Guid from, Guid to, int amount, int fee)
        {
            var sender = await context.Users.FirstOrDefaultAsync(s => s.Id == from);
            var receiver = await context.Users.FirstOrDefaultAsync(s => s.Id == to);
            var totalAmount = amount + fee;

            using (var tr = await context.Database.BeginTransactionAsync())
            {
                sender.Balance -= totalAmount;
                receiver.Balance += totalAmount;

                var transaction = await context.Transactions.AddAsync(new TransactionModel(from, to, TransactionType.Transfer, amount, fee));

                var changes = await context.SaveChangesAsync();

                if (changes != 3)
                {
                    tr.Commit();
                    return transaction.Entity;
                }

                throw new ApplicationException("Не удалось совершить транзакцию");
            }
        }

        public async Task<IEnumerable<TransactionModel>> GetUserTransactionsAsync(Guid userId, int offset, int count)
        {
            return await context.Transactions.AsNoTracking().Where(s => s.From == userId || s.To == userId).Skip(offset).Take(count).ToListAsync();
        }
    }
}