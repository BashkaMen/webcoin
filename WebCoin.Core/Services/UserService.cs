﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using WebCoin.Models;

namespace WebCoin.Core.Services
{
    public class UserService
    {
        private readonly MainContext context;

        public UserService(MainContext context)
        {
            this.context = context;
        }

        public async Task<UserModel> GetOrCreate(string phone)
        {
            var user = await context.Users.AsNoTracking().FirstOrDefaultAsync(s => s.Login == phone);

            if (user == null)
            {
                var entry = await context.Users.AddAsync(new UserModel(phone, Role.User));
                await context.SaveChangesAsync();
                user = entry.Entity;
            }

            return user;
        }

        public async Task<bool> HasOnBalanceAsync(Guid userId, int amount)
        {
            var user = await context.Users.AsNoTracking().FirstOrDefaultAsync(s => s.Id == userId);

            return user.Balance >= amount;
        }

        public Task<bool> UserExistAsync(Guid userId)
        {
            return context.Users.AsNoTracking().AnyAsync(s => s.Id == userId);
        }

        public async Task<int> GetBalanceAsync(Guid userId)
        {
            var user = await context.Users.AsNoTracking().FirstOrDefaultAsync(s => s.Id == userId);

            return user.Balance;
        }
    }
}